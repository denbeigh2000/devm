/*
 * polling.h
 * Functions used to get data from
 * "leopard" and "mouse"
 * Denbeigh Stevens
 * COMP2129 - Assignment 5
 * University of Sydney
 */

/*
 * Reads a three-byte size packet from "mouse"
 * dev_path: path to device
 * buffer: an array of 3 unsigned chars
 */
void
get_mouse_data(FILE** dev_path, unsigned char* buffer)
{
    int read_status = 0;
    for (int i = 0; i < 3; i++) {
        read_status = fread(&buffer[i], 1, 1, *dev_path);
        if ((i == 0) && (read_status == 0))
        {
            /*expected disconnect */
            buffer[0] = 'z';
            return;
        }
        if ((i != 0) && (read_status == 0))
        {
            /* unexpected disconnect */
            buffer[0] = 'z';
            buffer[1] = 'z';
            return;
        }
    }
}

/*
 * Reads a byte-sized packet from "keyboard"
 * dev_path: path to device
 * buffer: pointer to a single unsigned char for the data to be read into
 */
void
get_keyboard_data(FILE* dev_path, unsigned char* buffer)
{
    int read_status = 0;
    read_status = fread(buffer, 1, 1, dev_path);
    //*buffer = (unsigned char) fgetc(dev_path);
    //FILE* log = open_log();
    //int unix_time = (int)time(NULL);
    //fprintf(log, "[%d] Device packet: 0x%.2x from keyboard", unix_time, *buffer);
    //fclose(log);
    //if (*buffer == EOF)
    if (read_status == 0)
        *buffer = 'z';
}
