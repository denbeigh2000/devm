#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "linked_list.h"
#include "decode_device.h"
#include "misc.h"
#include "logging.h"
#include "polling.h"

#define KNOWN_DEV "known_devices"
#define NEW_DEV "new_devices"
#define PID_FILE "devm.pid"
#define BUFFER_SIZE 256
#define PACKET_REC "Device packet: %x from %s %s at %s"
#define SIGTERM_MSG "SIGTERM caught, devm closing"

/* Concerning logging and start of program */
list* known_dev;
FILE* open_log();
FILE* new_dev_file = NULL;
void start();
void write_pid();
list* read_known();
int num_devices = 0;
void dev_monitor(device* dev, int dev_num);
void bail(int sig, siginfo_t * info, void * data);
void* salloc(size_t nmemb, size_t size);
device* this_dev = NULL;
int this_dev_num = -1;
int sig_handle(int sig, void (*h) (int, siginfo_t *, void *));

FILE* dev_in = NULL;
FILE* dev_out = NULL;

char** known_devices;
pid_t* kiddies; /* Holds the PIDs of our child processes */

/* concerning catching signals */

int find_pid(pid_t pid);

/* "When life gives you lemons, just say "fuck the lemons" and bail." */
/* Signal handler for SIGTERM
 * Some of these variables aren't used, but
 * it needs to be that way to satisfy sigaction()
 */
void
bail(int sig, siginfo_t * info, void * data)
{
    /* Log sigterm */
    log_msg(SIGTERM_MSG);

    /* kill child processes */
    for (int i = 0; i < known_dev->length; i++) {
        if (kiddies[i] != 0) {
            kill(kiddies[i], SIGTERM);
        }
    }

    /* no memory leaks here */
    free(kiddies);
    fclose(new_dev_file);
    dev_list_destroy(known_dev);
    remove(PID_FILE);
    exit(0);
}

/*
 * returns the PID of the parent process
 */
pid_t
get_parent()
{
    FILE* pid_file = fopen(PID_FILE, "r");
    pid_t pid = 0;
    fscanf(pid_file, "%d\n", &pid);
    fclose(pid_file);
    return pid;
}

/*
 * EOF caller for dev_monitor
 * Closes child processes
 * Sends signal to parent to
 * zero out its PID
 */
void
child_bail(int send_sig)
{
    if (send_sig == 1)
    {
        kill(get_parent(), SIGCHLD);
    }
    fclose(dev_in);
    fclose(dev_out);
    _exit(0);
}

/*
 * SIGTERM handler for child process
 * Sends signal to parent if the parent didn't
 * shut this process down
 */
void
child_sigterm(int sig, siginfo_t * info, void * data)
{
    pid_t par = get_parent();
    if (info->si_pid != par)
    {
        /* Sigterm is from another process, send SIGCHLD to parent */
        child_bail(1);
    } else {
        /* Sigterm is from parent, this means the parent is shutting down anyway */
        child_bail(0);
    }
}

/*
 * Helper function
 * To determine if this process is the parent
 * (cuz hey, who knows?)
 */
int
is_parent()
{
    return (getpid() == get_parent());
}

/*
 * Opens the new_devices file safely
 */
FILE*
get_new_devices()
{
    FILE* new_dev = fopen(NEW_DEV, "r");
    if (new_dev == NULL) {
        perror("Something went wrong opening new_devices.");
        exit(1);
    }

    return new_dev;
}

/*
 * Signal handler for connecting a device
 */
void
handle_connect()
{
    /* ID of newly connected device */
    char id[11];
    /* get file info */

    /* Get info about device and log it */
    char* getresult = fgets(id, sizeof(id), new_dev_file);
    device* found = find_device(known_dev, id);
    char type[9];
    if (found == NULL)
    {
        return;
        /* We don't have a known device matching this ID, flog it off */
    }

    /* now we certainly have a device we know how to operate */
    char* msg[BUFFER_SIZE];
    if (found->type == 'k') {
        strcpy(type, "keyboard");
    } else {
        strcpy(type, "mouse");
    }
    /* Gotta log it */
    sprintf((char *)msg, "Device connected: %s %s at dev/USB%d", type, found->id, num_devices);
    log_msg((char *)msg);


    /* make child process to handle new data */
    pid_t pid = fork();
    if (pid == -1)
    {   /* error - no child created */
        perror("No child created, system fail");
    }
    else if (pid == 0)
    {   /* child process */
        dev_monitor(found, num_devices);
    } else {
        /* parent process - log PID */
        kiddies[num_devices] = pid;
        num_devices++;
        return;
    }
}

/*
 * Signal handler for SIGCHLD
 * Zero out a PID entry
 */
void
signal_child(int sig, siginfo_t * info, void * data)
{
    if (!is_parent())
    {   /* We only want action if we are the parent. */

        return;
    }

    pid_t sender_pid = info->si_pid;
    /* find the PID in our known_array of children */
    int dev_num = find_pid(sender_pid);
    if (dev_num == -1) {
        return;
        //random SIGCHLD signal, do nothing
    } else {
        /* this child process is finished, get rid of its PID */
        kiddies[dev_num] = 0;
    }
}



/*
 * Helper function
 * make sure the device contains the string "mouse" or "keyboard"
 */
char check_device(char* desc) {
    if (strcasestr(desc, "keyboard") != NULL) {
        return 'k';
    } else if (strcasestr(desc, "mouse") != NULL) {
        return 'm';
    } else {
        return 'a';
    }
}

/*
 * Reads known_devices
 * Returns a pointer to a
 * linked list (linked_list.h)
 */
list*
read_known()
{
    /* get things happening */
    list* known_devices = make_dev_list();
    FILE* known = fopen(KNOWN_DEV, "r");
    if (known == NULL) {
        log_msg("Couldn't open known_devices for reading.");
        bail(0, NULL, 0);
    }

    char input[BUFFER_SIZE];
    char id[10];
    char* getresult = fgets(input, sizeof(input), known);

    /* iterate over each line in the known_dev file */
    while (getresult != NULL)
    {
        sscanf(input, "ID %s\n", (char *)&id);
        char dt = check_device(input);
        /* add this to our list of known devices */
        if (dt != 'a')
        {
            device* nd = make_dev(id, dt);
            dev_list_add_end(known_devices, nd);
        }
        getresult = fgets(input, sizeof(input), known);
    }
    fclose(known);
    return known_devices;
}

/*
 * Child function
 * Monitors the device, logs and outputs
 */
void
dev_monitor(device* dev, int dev_num)
{
    char dev_path[BUFFER_SIZE];
    char out_path[BUFFER_SIZE];
    sprintf(dev_path, "dev/USB%d", dev_num);
    sprintf(out_path, "output/USB%d", dev_num);
    sig_handle(SIGTERM, child_sigterm);

    /* set global variables */
    this_dev = dev;
    this_dev_num = dev_num;
    dev_in = fopen(dev_path, "rb");
    dev_out = fopen(out_path, "w");

    /* mouse monitoring */
    if (dev->type == 'm')
    {
        mouse_data data;
        unsigned char mouse_buffer[3];
        get_mouse_data(&dev_in, mouse_buffer);

        while (mouse_buffer[0] != 'z')
        {
            log_packet(mouse_buffer, dev, dev_num); // log packet to devm.log
            data = decode_mouse(mouse_buffer); // put into meaningful data
            log_mouse_data(&data, dev_out); // log meaningful data
            get_mouse_data(&dev_in, mouse_buffer); // get next packet
        }

        eof_handle(mouse_buffer, dev, dev_num);
        child_bail(1);
    }

    /* keyboard monitoring */
    else
    {
        keyboard_data data;
        data.caps = 0;
        unsigned char key_data;

        /* get packet from keyboard */
        get_keyboard_data(dev_in, &key_data);
        while (key_data != 'z')
        {
            log_packet(&key_data, dev, dev_num); // devm.log
            data = decode_keyboard(data.caps, key_data); // meaningful data
            log_keyboard_data(data.key, dev_out); // output/USBx
            get_keyboard_data(dev_in, &key_data); // dev/USBx
        }

        eof_handle(&key_data, dev, dev_num);
        child_bail(1);
    }
}

/*
 * Safely allocate zero-ed out memory */
void *
salloc(size_t nmemb, size_t size)
{
    void* t = calloc(nmemb, size);
    if (t == NULL)
    {
        perror("Unable to allocate memory");
        bail(0, NULL, 0);
    }
    return t;
}

/* Record initial PID of program and log start message */
void
start()
{
    log_msg("devm started");
    write_pid();
    known_dev = read_known();
    kiddies = (pid_t *) salloc(known_dev->length, sizeof(pid_t));
    /* we want to allocate each PID entry so as to be zero */
    for (int i = 0; i < known_dev->length; i++) {
        kiddies[i] = 0;
    }
}

/* Write the PID to a file */
void
write_pid() {
    pid_t pid = getpid();
    FILE* pid_file = fopen(PID_FILE, "w");
    fprintf(pid_file, "%d\n", pid);
    fclose(pid_file);
}

int
find_pid(pid_t pid)
{
    for (int i = 0; i < known_dev->length; i++) {
        if (kiddies[i] == pid)
        {
            return i;
        }
    }
    return -1; // no child found, we've received a random signal
}

int sig_handle(int sig, void (*h) (int, siginfo_t *, void *))
{
    int r;
    struct sigaction s;
    s.sa_flags = SA_SIGINFO;
    s.sa_sigaction = h;
    sigemptyset(&s.sa_mask);
    r = sigaction(sig, &s, NULL);
    if (r < 0) perror(__func__);
    return r;
}

/**
 * Main function
 */
int main(int argc, char** argv)
{
    start();
    new_dev_file = get_new_devices();
    /* Attach handlers functions to signals */
    signal(SIGUSR1, handle_connect);
    sig_handle(SIGTERM, bail);
    sig_handle(SIGCHLD, signal_child);

    for(;;)
    {
        sleep(1);
    }

    return 0;
}
