#include <stdio.h>
#define BUFFER_SIZE 256
#define LOG_FILE "devm.log"
#define LOG_MOUS "dx: %s, dy: %s, sc: %s, left: %s, middle: %s, right: %s\n"

int is_parent();

/* Open the log file for appending */
FILE*
open_log() {
    FILE* temp = fopen(LOG_FILE, "a");
    if (temp == NULL) {
        perror("Unable to open logfile for writing!");
        exit(1);
    } else {
        return temp;
    }
}

void
log_msg(char* msg)
{
    FILE* log = open_log();
    int unix_time = (int)time(NULL);
    fprintf(log,"[%d] %s\n", unix_time, msg);
    fclose(log);
}

void
log_packet(unsigned char * buffer, device* dev, int dev_num)
{
    char msg[BUFFER_SIZE];
    if (dev->type == 'm')
    {
        sprintf(msg, "Device packet: 0x%.2x%.2x%.2x from mouse %s at dev/USB%d",
                buffer[0], buffer[1], buffer[2], dev->id, dev_num);
    } else {
        sprintf(msg, "Device packet: 0x%.2x from keyboard %s at dev/USB%d",
                *buffer, dev->id, dev_num);
    }
    log_msg(msg);
}

void
log_disconnect(device* dev, int dev_num)
{
    char disconnect_str[75];
    char type[9];
    strcpy(type, get_dev_type(dev->type));

    sprintf(disconnect_str, "Device disconnected: %s %s at dev/USB%d", type, dev->id, dev_num);
    log_msg(disconnect_str);
}

void
log_error(device* dev, int dev_num)
{
    char error_str[70]; //some padding for large device numbers (dev/usb99999)
    char type[9];
    strcpy(type, get_dev_type(dev->type));

    sprintf(error_str, "Error while receiving packet: %s %s at dev/USB%d", type, dev->id, dev_num);
    log_msg(error_str);
    log_disconnect(dev, dev_num);
}

/*
 * Writes output from mouse to the
 * given (open) file
 */
void
log_mouse_data(mouse_data* data, FILE* output)
{
    fprintf(output, LOG_MOUS, data->dx, data->dy, data->scroll, data->left, data->middle, data->right);
}

void
log_keyboard_data(char keystroke, FILE* output)
{
    fprintf(output, "%c\n", keystroke);
}

void
eof_handle(unsigned char* buffer, device* dev, int dev_num)
{
    if (dev->type == 'k')
    {
        log_disconnect(dev, dev_num);
    }
    else
    {
        if (buffer[1] == 'z')
        {
            log_error(dev, dev_num);
        } else {
            log_disconnect(dev, dev_num);
        }
    }
}
