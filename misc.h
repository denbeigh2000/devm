char*
get_dev_type(char type)
{
    if (type == 'k')
    {
        return "keyboard";
    } else {
        return "mouse";
    }
}
