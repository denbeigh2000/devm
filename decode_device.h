#include <stdio.h>
#include <string.h>

/* hope you like masks */
/* mouse masks */
#define MASK_FLAGS 0xff0000
#define MASK_XOVER 0x80
#define MASK_YOVER 0x40
#define MASK_YSIGN 0x20
#define MASK_XSIGN 0x10
#define MASK_MIDDL 0x05
#define MASK_RIGHT 0x02
#define MASK_LEFT  0x01

//#define MASK_DY    0x3fc00
//#define MASK_DX    0x3fc

#define MASK_DY    0xfe
#define MASK_DX1   0x01
#define MASK_DX2   0xfc
#define MASK_SCROL 0x03

/* keyboard masks */
#define MASK_CAPS  0x80
#define MASK_SHIFT 0x40
#define MASK_KEYPR 0x3f

#define SHIFT_DIFF 46

const char CHAR_SET[] =
    "0123456789" // {0-9}
    "-=[]\\;',./" // {10-19}
    "abcdefghijklmnopqrstuvwxyz" // {20-45}
    /* standard set end */
    ")!@#$%^&*("
    "_+{}|:\"<>?"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
/* shifted set end */

typedef struct mouse_data mouse_data;
typedef struct keyboard_data keyboard_data;

struct
        mouse_data
{
//  int y_over;
//  int x_over;
//  int y_sign;
//  int x_sign;
    char middle[4]; //can hold up to "off\0"
    char right[4];
    char left[4];
    char dy[5]; // can hold up to "-127\0"
    char dx[5];
    char scroll[3]; //0, 1, 2

};

struct
        keyboard_data
{
    int caps; /* returns the status of the CAPS LOCK key after the operation */
    char key; /* ASCII value of the key pressed */
    /* shift does not need to be returned, it is handled in the function */
};

mouse_data
decode_mouse(unsigned char * data)
{

    unsigned char flags = data[0];
    mouse_data return_me;

    /* FIRST BYTE: FLAGS */
    //return_me.y_over = (int) (flags & MASK_XOVER) >> 7;
    //return_me.x_over = (int) (flags & MASK_YOVER) >> 6;
    int y_sign = (int) (flags & MASK_YSIGN) >> 5;
    int x_sign = (int) (flags & MASK_XSIGN) >> 4;
    int middle = (int) (flags & MASK_MIDDL) >> 2;
    int right  = (int) (flags & MASK_RIGHT) >> 1;
    int left   = (int) (flags & MASK_LEFT);

    /* SECOND TWO BYTES: MOVEMENT */
    //return_me.dy     = (int) (data & MASK_DY) >> 9;
    int dy     = (int) (data[1] & MASK_DY) >> 1;
    /* finding dx */
    unsigned char dx1 = (data[1] & MASK_DX1) << 6;
    unsigned char dx2 = (data[2] & MASK_DX2) >> 2;
    int dx = (int) (dx1 | dx2);
    //return_me.dx     = (int) (data & MASK_DX) >> 2;
    int tempscr = (int) (data[2] & MASK_SCROL);

    if (y_sign)
        sprintf(return_me.dy, "-%d",dy);
    else
        sprintf(return_me.dy, "+%d",dy);

    if (x_sign)
        sprintf(return_me.dx, "-%d",dx);
    else
        sprintf(return_me.dx,"+%d",dx);

    if (left)
        strcpy(return_me.left, "on");
    else
        strcpy(return_me.left, "off");

    if (right)
        strcpy(return_me.right, "on");
    else
        strcpy(return_me.right, "off");

    if (middle)
        strcpy(return_me.middle, "on");
    else
        strcpy(return_me.middle, "off");

    switch (tempscr) {
    case 0:
        strcpy(return_me.scroll, "+0");
        break;

    case 1:
        strcpy(return_me.scroll, "-1");
        break;

    case 2:
        strcpy(return_me.scroll, "+1");
        break;
    }
    return return_me;
}

int
check_error(unsigned char* val)
{
    if ((val[0] == EOF) || (val[1] == EOF) || (val[2] == EOF))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int
toggle(int i)
{
    if (i == 1)
    {
        return 0;
    } else {
        return 1;
    }
}

int
apply_caps(int x)
{
    if ((x >= 20) && (x <= 45))
    {
        return x + SHIFT_DIFF;
    }
    else if ((x >= 66) && (x <= 91))
    {
        return x - SHIFT_DIFF;
    }
    else
    {
        return x;
    }
}

keyboard_data
decode_keyboard(int prev_caps, unsigned char key_data)
{
    keyboard_data return_me;
    int caps       = (int) (key_data & MASK_CAPS) >> 7;
    int shift      = (int) (key_data & MASK_SHIFT) >> 6;
    int raw_keypr  = (int) (key_data & MASK_KEYPR);
    if (shift == 1)
    {
        raw_keypr = raw_keypr + SHIFT_DIFF;
    }

    if (caps == 1)
    {
        //printf("Caps has been toggled!\n");
        return_me.caps = toggle(prev_caps);
    } else {
        return_me.caps = prev_caps;
    }

    if (return_me.caps)
    {
        //printf("Caps lock is actually on.\n");
        raw_keypr = apply_caps(raw_keypr);
    }

    return_me.key = CHAR_SET[raw_keypr];
    return return_me;
}

//void
//main()
//{
//  /* ARRAY OF SAMPLE DATA */
//  unsigned char val[3] = {0x2a, 0x09, 0x02};
//  mouse_data asd = decode_mouse(val);
//  printf("dx = %d\ndy = %d\nsc = %d\nleft = %d\nmid = %d\nright = %d\n",asd.dx,asd.dy,asd.scroll,asd.left,asd.middle,asd.right);
//  printf("%c\n",decode_keyboard(1, 0x14).key);
//}
