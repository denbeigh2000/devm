typedef struct node node;
typedef struct list list;
typedef struct device device;

struct list {
    node* head;
    node* tail;
    int length;
};

struct
        node
{
    device* dev;
    node* next;
    node* prev;
};

struct
        device
{
    char id[10];
    char type;
};

device*
find_device(list* dev_list, char* id)
{
    node* current = dev_list->head;
    while (current != NULL) {
        if (strncmp(id, current->dev->id, 9) == 0)
        {
            return current->dev;
        }
        else
        {
            current = current->next;
        }
    }
    return NULL; /* device not found */

}

device*
make_dev(char* id, char dt)
{
    device* d = (device *) malloc(sizeof(device));
    if (d != NULL)
    {
        strcpy(d->id, id);
        if ((dt == 'm') || (dt == 'k')) {
            d->type = dt;
        } else {
            // make_dev should only be called if we have a device with mouse/keyboard
            // Just here for consistency
            free(d); //free the "D" ;)
            return NULL;
        }
    }
    //printf("Made new device: %c with ID %s\n",dt,id);

    return d;
}

list*
make_dev_list()
{
    list* l = (list *) malloc(sizeof(list));
    /* we don't want any invalid references. */
    l->head = NULL;
    l->tail = NULL;
    l->length = 0;
    return l;
}

void
dev_list_destroy(list* l)
{
    node* n = l->head;
    while (n != NULL)
    {
        free(n->dev);
        node* next  = n->next;
        free(n);
        n = next;
    }
    free(l);
}

node*
dev_list_add_front(list* l, device* d)
{
    /* allocate space in memory for node */
    node* new_node = (node *) malloc(sizeof(node));
    if (new_node == NULL)
    {
        return NULL;
    }

    /* attach the device to the new node */
    new_node->dev = d;
    if (l->head == NULL) /* empty list */
    {
        new_node->next = NULL;
        l->head = new_node;
        l->tail = new_node;
    }
    else
    {
        new_node->next = l->head;
        l->head = new_node;
    }

    l->length++;
    return l->head;
}

node*
dev_list_add_end(list* l, device* d)
{
    if (l->head == NULL)
        return dev_list_add_front(l,d);

    node* new_node = (node *) malloc(sizeof(node));
    new_node->dev = d;
    new_node->prev = l->tail;
    new_node->next = NULL;
    l->tail->next = new_node;
    l->tail = new_node;
    l->length++;
    return l->tail;
}
